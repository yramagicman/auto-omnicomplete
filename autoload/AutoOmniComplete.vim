"{{{ configure global variables that need to be initialized immmediately
if ! exists('g:complete_timers')
    let g:complete_timers = 0
endif
if ! exists('g:completion_delay')
    let g:completion_delay = 100
endif
"}}}
"{{{ tab completion
function! AutoOmniComplete#InsertTabWrapper()
    let col = col('.') - 1
    if !col || getline('.')[col - 1] !~ '\k'
        return "\<tab>"
    elseif pumvisible()
        return "\<C-n>"
    else
        return "\<C-n>"
    endif
endfunction
"}}}
"{{{ open omni complete pop up
let g:_calls = 0
function! AutoOmniComplete#insertComplete()
    function! Showmenu(t)
        if ! pumvisible() && v:insertmode == 'i'
            if ! exists('g:AutoOmniComplete_complete_map')
                silent! call feedkeys("\<c-x>\<c-o>")
            else
                silent! call feedkeys(g:AutoOmniComplete_complete_map)
            endif
        endif
        let g:_calls = g:_calls + 1
    endfunction
    let l:col = col('.') - 1
    if ! pumvisible() && getline('.')[l:col - 1] =~ '\k' && g:_calls < 1
        call AutoOmniComplete#clearTimer()
        let g:complete_timers  = timer_start(g:completion_delay, 'Showmenu', {'repeat': 1})
    endif
    if getline('.')[l:col - 1] !~ '\k'
        let g:_calls = 0
    endif
endfunction
"}}}
"{{{ Bind key to switch to keyword completion
function! AutoOmniComplete#BindCompleteKeyword(bind)
    let g:AutoOmniComplete_keyword = a:bind
    execute 'inoremap <expr> ' . g:AutoOmniComplete_keyword . ' AutoOmniComplete#CompleteKeyword()'
endfunction

function! AutoOmniComplete#CompleteKeyword()
    if pumvisible()
        return "\<c-x>\<c-n>\<c-n>"
    else
        if exists('g:complete_keword')
            return g:complete_keword
        endif
    endif
endfunction
"}}}
"{{{ Bind key to switch to tag completion
function! AutoOmniComplete#BindCompleteTag(bind)
    let g:AutoOmniComplete_tag = a:bind
    execute 'inoremap <expr> ' . g:AutoOmniComplete_tag . ' AutoOmniComplete#CompleteTag()'
endfunction

function! AutoOmniComplete#CompleteTag()
    if pumvisible()
        return "\<c-x>\<c-]>\<c-n>"
    else
        if exists('g:AutoOmniComplete_tag')
            return g:AutoOmniComplete_tag
        endif
    endif
endfunction
"}}}
"{{{ Bind key to switch to file completion
function! AutoOmniComplete#BindCompleteFile(bind)
    let g:AutoOmniComplete_file = a:bind
    execute 'inoremap <expr> ' . g:AutoOmniComplete_file . ' AutoOmniComplete#CompleteFile()'
endfunction

function! AutoOmniComplete#CompleteFile()
    if pumvisible()
        return "\<c-x>\<c-f>\<c-n>"
    else
        if exists('g:AutoOmniComplete_file')
            return g:AutoOmniComplete_file
        endif
    endif
endfunction
"}}}
"{{{ Bind key to switch to omni completion
function! AutoOmniComplete#BindCompleteOmni(bind)
    let g:AutoOmniComplete_omni = a:bind
    execute 'inoremap <expr> ' . g:AutoOmniComplete_omni . ' AutoOmniComplete#CompleteOmni()'
endfunction

function! AutoOmniComplete#CompleteOmni()
    if pumvisible()
        return "\<c-x>\<c-o>\<c-n>"
    else
        if exists('g:AutoOmniComplete_omni')
            return g:AutoOmniComplete_omni
        endif
    endif
endfunction
"}}}
"{{{ clear timer for completions
function! AutoOmniComplete#clearTimer()
    if g:complete_timers > 0
        call timer_stop(g:complete_timers)
    endif
    if exists('g:complete_timers')
        let g:complete_timers = 0
    endif
endfunction
"}}}
