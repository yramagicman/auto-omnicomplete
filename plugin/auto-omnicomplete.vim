if !exists('g:AutoOmniComplete_tab')
    let g:AutoOmniComplete_tab = 0
endif

if g:AutoOmniComplete_tab == 1
    inoremap <expr> <tab> AutoOmniComplete#InsertTabWrapper()
endif

if !exists('g:AutoOmniComplete_tag')
    call AutoOmniComplete#BindCompleteTag("\<c-t>")
else
    call AutoOmniComplete#BindCompleteTag(g:AutoOmniComplete_tag)
endif
if !exists('g:AutoOmniComplete_keyword')
    call AutoOmniComplete#BindCompleteKeyword("\<c-k>")
else
    call AutoOmniComplete#BindCompleteKeyword(g:AutoOmniComplete_keyword)
endif
if !exists('g:AutoOmniComplete_file')
    call AutoOmniComplete#BindCompleteFile("\<c-f>")
else
    call AutoOmniComplete#BindCompleteFile(g:AutoOmniComplete_file)
endif

if !exists('g:AutoOmniComplete_omni')
    call AutoOmniComplete#BindCompleteOmni("\<c-o>")
else
    call AutoOmniComplete#BindCompleteOmni(g:AutoOmniComplete_omni)
endif

if g:AutoOmniComplete_tab == 1
    inoremap <expr><S-TAB>  pumvisible() ? "\<C-p>" : "\<S-TAB>"
endif

augroup AutoOmniComplete
    autocmd!
    autocmd CursorMovedI * nested call AutoOmniComplete#insertComplete()
    autocmd InsertLeave * nested call AutoOmniComplete#clearTimer()
augroup end
